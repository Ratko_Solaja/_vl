<?php
/**
 * _VL API Class
 *
 * @since   1.0.0
 * @package _vl
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( '_VL_API' ) )
{

    /**
     * The Main _VL_API Class
     */
    class _VL_API {

        /**
         * Setup Class
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function __construct()
        {

            $this->_vl_maybe_init_api();

        }

        /**
         * Maybe initialize api.
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function _vl_maybe_init_api()
        {

            // REST API was included starting WordPress 4.4.
            if ( ! class_exists( 'WP_REST_Server' ) ) {

                return;

            }

            $this->_vl_require_api_files();

            add_action( 'rest_api_init', [ $this, '_vl_rest_api_init' ], 10 );

        }

        /**
         * Require API Files.
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function _vl_require_api_files()
        {

            //require_once get_theme_file_path( 'inc/api/class-vl-api-example-controller.php' );

        }

        /**
         * Initialize API
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function _vl_rest_api_init()
        {

            $controllers = [
                //'_VL_API_Example_Controller'
            ];

            if ( isset( $controllers ) && ! empty( $controllers ) ) {

                foreach ( $controllers as $controller ) {

                    $this->$controller = new $controller();
                    $this->$controller->register_routes();

                }

            }

        }

    }

}

return new _VL_API();